(function () {

	var tictactoe = function ( el ) {

		this.state = "playing";
		this.turn = "X";

		this.props = {};

		this.props.beginPlayer = tictactoe.beginPlayer;

		this.props.playerX = "human";
		this.props.playerO = "computer";

		this.props.rows = tictactoe.rows;
		this.props.cols = tictactoe.cols;
		this.props.linetowin = tictactoe.linetowin;

		this.props.sqcount = 0;
		this.props.sqcaptured = 0;

		this.elements = {};

		this.elements.container = null;
		if ( typeof el == 'object' && el instanceof HTMLElement ) this.elements.container = el;
		else this.elements.container = document.createElement('div');
		this.elements.container.classList.add(tictactoe.className);
		this.elements.container.dataTTTObject = this;

		this.elements.controls = document.createElement('div');
		this.elements.controls.classList.add(tictactoe.controlsClassName);
		this.elements.controls.dataTTTObject = this;
		this.elements.container.appendChild(this.elements.controls);

		this.elements.playerSymbol = document.createElement('div');
		this.elements.playerSymbol.classList.add(tictactoe.playerSymbolClassName);
		this.elements.playerSymbol.dataTTTObject = this;
		this.elements.controls.appendChild(this.elements.playerSymbol);

		this.elements.restartbtn = document.createElement('button');
		this.elements.restartbtn.classList.add(tictactoe.restartbtnClassName);
		this.elements.restartbtn.dataTTTObject = this;
		this.elements.controls.appendChild(this.elements.restartbtn);
		this.elements.restartbtn.addEventListener('click', function ( e ) {
			var self = e.target.dataTTTObject;
			self.reset();
		});

		this.elements.table = document.createElement('table');
		this.elements.table.dataTTTObject = this;
		this.elements.container.appendChild(this.elements.table);

		this.elements.tbody = document.createElement('tbody');
		this.elements.tbody.dataTTTObject = this;
		this.elements.table.appendChild(this.elements.tbody);

		this.elements.winLine = document.createElement('div');
		this.elements.winLine.dataTTTObject = this;
		this.elements.winLine.style.display = "none";
		this.elements.winLine.style.position = "relative";
		this.elements.winLine.style.overflow = "hidden";
		this.elements.winLine.style.boxShadow = "0px 0px 3px 2px #000";
		this.elements.container.appendChild(this.elements.winLine);

		this.elements.status = document.createElement('div');
		this.elements.status.dataTTTObject = this;
		this.elements.container.appendChild(this.elements.status);

		var tmptr, tmptd;

		for ( var y = 0; y < this.props.rows; y++ ) {
			tmptr = document.createElement('tr');
			tmptr.dataTTTObject = this;
			this.elements.tbody.appendChild(tmptr);

			for ( var x = 0; x < this.props.cols; x ++ ) {
				tmptd = document.createElement('td');
				tmptd.dataTTTObject = this;
				tmptd.dataCaptured = false;
				tmptd.dataPlayer = '';
				tmptd.dataY = y;
				tmptd.dataX = x;
				tmptr.appendChild(tmptd);
				this.props.sqcount++;
			}
		}

		this.ai = function () {
			if ( this.props["player" + this.turn] !== "computer" ) return false;
			var p = this.points();
			var pd = [];

			for ( var y = 0; y < p.length; y ++ )
				for ( var x = 0; x < p[y].length; x ++ ) if ( p[y][x].cell.dataCaptured === false ) {
					pd.push( p[y][x] );
				}

			pd.sort(function(a, b) { return b.points - a.points; });

			if ( pd.length == 0 ) return false;
			var pts = pd[0].points;
			for ( var y = 0; y < pd.length; y ++ ) if ( pts > pd[y] ) { pd.splice(y,pd.length - y); break; }

			console.log ( pd ) ;
			this.captureBox(pd[0].cell);
			
		}

		this.points = function () {
			var CurrentPlayerPoint = 1.0;
			var OtherPlayerPoint = 2.0;

			function d ( c, t ) {
				var cpoints = 0;
				var d = {
					'u'  : {c:c,p:0,ps:null,nc:0,no:0},
					'ul' : {c:c,p:0,ps:null,nc:0,no:0},
					'ur' : {c:c,p:0,ps:null,nc:0,no:0},
					'l'  : {c:c,p:0,ps:null,nc:0,no:0},
					'r'  : {c:c,p:0,ps:null,nc:0,no:0},
					'd'  : {c:c,p:0,ps:null,nc:0,no:0},
					'dl' : {c:c,p:0,ps:null,nc:0,no:0},
					'dr' : {c:c,p:0,ps:null,nc:0,no:0}
				};
				var pe; // parent element
				var j = {};

				for ( var i = 0; i < t.props.linetowin-1; i ++ ) {
					// for ( i = t.props.linetowin - 2; i > 0; i -- ) {

					// DOWN
					if ( d.d.c !== false ) {
						pe = d.d.c.parentElement.nextElementSibling;
						if ( pe === null ||
							 pe.children[d.d.c.cellIndex].dataPlayer === false
						   ) d.d.c = false;
						else {
							d.d.c = pe.children[d.d.c.cellIndex];
							if ( d.d.c.dataPlayer === t.turn ) {
								d.d.nc ++;
								cpoints += (i + 1) * CurrentPlayerPoint;
								cpoints = ( d.d.c.dataPlayer === d.d.ps ) ? cpoints * 2 : cpoints / 2;
								d.d.ps = d.d.c.dataPlayer;
								d.d.p += CurrentPlayerPoint;
							}
							else if ( d.d.c.dataPlayer !== t.turn ) {
								d.d.no ++;
								cpoints += (i + 1) * OtherPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.d.p += OtherPlayerPoint;
							}
						}
					}

					// UP
					if ( d.u.c !== false ) {
						pe = d.u.c.parentElement.previousElementSibling;
						if ( pe === null ||
							 pe.children[d.u.c.cellIndex].dataPlayer === false
						   ) d.u.c = false;
						else {
							d.u.c = pe.children[d.u.c.cellIndex];
							if ( d.u.c.dataPlayer === t.turn ) {
								d.d.nc ++;
								cpoints += (i + 1) * CurrentPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.u.p += CurrentPlayerPoint;
							}
							else if ( d.u.c.dataPlayer !== t.turn ) {
								d.d.no ++;
								cpoints += (i + 1) * OtherPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.u.p += OtherPlayerPoint;
							}
						}
					}

					if ( d.d.ps == d.u.ps ) {
						if ( d.u.c.dataPlayer === t.turn ) {
							cpoints += (i + 1) * CurrentPlayerPoint;
							cpoints += (d.d.nc + d.u.nc) * CurrentPlayerPoint;
						} else if ( d.u.c.dataPlayer !== t.turn ) {
							cpoints += (i + 1) * OtherPlayerPoint;
							cpoints += (d.d.no + d.u.no) * OtherPlayerPoint;
						}
					}

					// RIGHT
					if ( d.r.c !== false ) {
						if ( d.r.c.nextElementSibling === null ||
							 d.r.c.nextElementSibling.dataPlayer === false
						   ) d.r.c = false;
						else {
							d.r.c = d.r.c.nextElementSibling;
							if ( d.r.c.dataPlayer === t.turn ) {
								d.d.nc ++;
								cpoints += (i + 1) * CurrentPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.r.p += CurrentPlayerPoint;
							}
							else if ( d.r.c.dataPlayer !== t.turn ) {
								d.d.no ++;
								cpoints += OtherPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.r.p += OtherPlayerPoint;
							}
						}
					}

					// LEFT
					if ( d.l.c !== false ) {
						if ( d.l.c.previousElementSibling === null ||
							 d.l.c.previousElementSibling.dataPlayer === false
						   ) d.l.c = false;
						else {
							d.l.c = d.l.c.previousElementSibling;
							if ( d.l.c.dataPlayer === t.turn ) {
								d.d.nc ++;
								cpoints += (i + 1) * CurrentPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.l.p += CurrentPlayerPoint;
							}
							else if ( d.l.c.dataPlayer !== t.turn ) {
								d.d.no ++;
								cpoints += (i + 1) * OtherPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.l.p += OtherPlayerPoint;
							}
						}
					}

					if ( d.r.ps == d.l.ps ) {
						if ( d.l.c.dataPlayer === t.turn ) {
							cpoints += (i + 1) * CurrentPlayerPoint;
							cpoints += (d.l.nc + d.r.nc) * CurrentPlayerPoint;
						} else if ( d.l.c.dataPlayer !== t.turn ) {
							cpoints += (i + 1) * OtherPlayerPoint;
							cpoints += (d.l.no + d.r.no) * OtherPlayerPoint;
						}
					}

					// DOWN RIGHT
					if ( d.dr.c !== false ) {
						pe = d.dr.c.parentElement.nextElementSibling;
						if ( pe === null ||
							 pe.children[d.dr.c.cellIndex].nextElementSibling === null ||
							 pe.children[d.dr.c.cellIndex].nextElementSibling.dataPlayer === false
						   ) d.dr.c = false;
						else {
							d.dr.c = pe.children[d.dr.c.cellIndex].nextElementSibling;
							if ( d.dr.c.dataPlayer === t.turn ) {
								d.d.nc ++;
								cpoints += (i + 1) * CurrentPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.dr.p += CurrentPlayerPoint;
							}
							else if ( d.dr.c.dataPlayer !== t.turn ) {
								d.d.no ++;
								cpoints += (i + 1) * OtherPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.dr.p += OtherPlayerPoint;
							}
						}
					}

					// UP LEFT
					if ( d.ul.c !== false ) {
						pe = d.ul.c.parentElement.previousElementSibling;
						if ( pe === null ||
							 pe.children[d.ul.c.cellIndex].previousElementSibling === null ||
							 pe.children[d.ul.c.cellIndex].previousElementSibling.dataPlayer === false
						   ) d.ul.c = false;
						else {
							d.ul.c = pe.children[d.ul.c.cellIndex].previousElementSibling;
							if ( d.ul.c.dataPlayer === t.turn ) {
								d.d.nc ++;
								cpoints += (i + 1) * CurrentPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.ul.p += CurrentPlayerPoint;
							}
							else if ( d.ul.c.dataPlayer !== t.turn ) {
								d.d.no ++;
								cpoints += (i + 1) * OtherPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.ul.p += OtherPlayerPoint;
							}
						}
					}

					if ( d.dr.ps == d.ul.ps ) {
						if ( d.ul.c.dataPlayer === t.turn ) {
							cpoints += (i + 1) * CurrentPlayerPoint;
							cpoints += (d.dr.nc + d.ul.nc) * CurrentPlayerPoint;
						} else if ( d.ul.c.dataPlayer !== t.turn ) {
							cpoints += (i + 1) * OtherPlayerPoint;
							cpoints += (d.dr.no + d.ul.no) * OtherPlayerPoint;
						}
					}

					// DOWN LEFT
					if ( d.dl.c !== false ) {
						pe = d.dl.c.parentElement.nextElementSibling;
						if ( pe === null ||
							 pe.children[d.dl.c.cellIndex].previousElementSibling === null ||
							 pe.children[d.dl.c.cellIndex].previousElementSibling.dataPlayer === false
						   ) d.dl.c = false;
						else {
							d.dl.c = pe.children[d.dl.c.cellIndex].previousElementSibling;
							if ( d.dl.c.dataPlayer === t.turn ) {
								d.d.nc ++;
								cpoints += (i + 1) * CurrentPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.dl.p += CurrentPlayerPoint;
							}
							else if ( d.dl.c.dataPlayer !== t.turn ) {
								d.d.no ++;
								cpoints += (i + 1 ) * OtherPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.dl.p += OtherPlayerPoint;
							}
						}
					}

					// UP RIGHT
					if ( d.ur.c !== false ) {
						pe = d.ur.c.parentElement.previousElementSibling;
						if ( pe === null ||
							 pe.children[d.ur.c.cellIndex].nextElementSibling === null ||
							 pe.children[d.ur.c.cellIndex].nextElementSibling.dataPlayer === false
						   ) d.ur.c = false;
						else {
							d.ur.c = pe.children[d.ur.c.cellIndex].nextElementSibling;
							if ( d.ur.c.dataPlayer === t.turn ) {
								d.d.nc ++;
								cpoints += (i + 1) * CurrentPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.ur.p += CurrentPlayerPoint;
							}
							else if ( d.ur.c.dataPlayer !== t.turn ) {
								d.d.no ++;
								cpoints += (i + 1) * OtherPlayerPoint;
								if ( d.d.c.dataPlayer === d.d.ps ) cpoints *= 2; else cpoints /= 2;
								d.d.ps = d.d.c.dataPlayer;
								d.ur.p += OtherPlayerPoint;
							}
						}
					}

					if ( d.ur.ps == d.dl.ps ) {
						if ( d.dl.c.dataPlayer === t.turn ) {
							cpoints += (i + 1) * CurrentPlayerPoint;
							cpoints += (d.ur.nc + d.dl.nc) * CurrentPlayerPoint;
						} else if ( d.l.c.dataPlayer !== t.turn ) {
							cpoints += (i + 1) * OtherPlayerPoint;
							cpoints += (d.ur.no + d.dl.no) * OtherPlayerPoint;
						}
					}

				}

				// c.title = cpoints;

				return cpoints;
			}

			var ret = [];
			for (var y = 0; y < this.props.rows; y++ )
				ret.push ( new Array ( this.props.cols ) );

			var row, cell, r;
			for ( y = 0; y < this.props.rows; y ++ ) {
				row = this.elements.tbody.children[y];
				for ( var x = 0; x < this.props.cols; x ++ ) {
					cell = row.children[x];
					ret[y][x] = {};
					ret[y][x].cell = cell;
					ret[y][x].points = 0;

					if ( cell.dataCaptured === false ) {
						ret[y][x].points = d(ret[y][x].cell, this);
						ret[y][x].cell.title = ret[y][x].points;
					}

				}
			}

			return ret;
		};

		this.gameEnd = function ( m ) {
			for ( var i = 0; i < this.elements.tbody.children.length; i ++ ) {
				for ( var j = 0; j < this.elements.tbody.children[i].children.length; j ++ ) {
					this.elements.tbody.children[i].children[j].removeEventListener('click', clickBox);
				}
			}

			if ( typeof m == 'object' ) {
				this.elements.status.innerText = m.symbol + " wins!";
			}
			else if ( m === false ) {
				this.elements.status.innerText = "Tie";
			}
		};

		this.nextPlayer = function ( dontChange ) {

			if ( dontChange !== true ) {
				this.elements.playerSymbol.classList.remove('p'+this.turn);
				this.turn = this.turn == "X" ? "O" : "X";
				this.elements.playerSymbol.classList.add('p'+this.turn);
				this.props.sqcaptured++;
			}

			this.elements.status.innerText = "Player " + this.turn + " turn";

			if ( this.props['player' + this.turn] == "human" ) {
				for ( var i = 0; i < this.elements.tbody.children.length; i ++ ) {
					for ( var j = 0; j < this.elements.tbody.children[i].children.length; j ++ ) {
						if ( this.elements.tbody.children[i].children[j].dataPlayer == '' ) {
							this.elements.tbody.children[i].children[j].addEventListener('click', clickBox);
						} else {
							this.elements.tbody.children[i].children[j].removeEventListener('click', clickBox);
						}
					}
				}
				if ( this.props.sqcaptured == this.props.sqcount||this.checkForWin()) { this.gameEnd(false); return; }
			} else if ( this.props['player' + this.turn] == "computer" ) {
				for ( var i = 0; i < this.elements.tbody.children.length; i ++ ) {
					for ( var j = 0; j < this.elements.tbody.children[i].children.length; j ++ ) {
						this.elements.tbody.children[i].children[j].removeEventListener('click', clickBox);
					}
				}
				if ( this.props.sqcaptured == this.props.sqcount||this.checkForWin()) this.gameEnd(false);
				else this.ai();
				this.nextPlayer();
			}

		};

		this.captureBox = function ( x, y ) {
			var box;
			if ( typeof x == 'object' && typeof y == 'undefined'  ) {
				if ( x instanceof HTMLElement ) {
					box = x;
				}
				else {
					console.error ( "Not an element" );
				}
			}
			else if ( typeof x == 'number' && typeof y == 'number' ) {
				if ( x < 0 ||  x >= this.props.cols ) {
					console.error ( "x out of range");
					return;
				}
				if ( y < 0 || y >= this.props.rows ) {
					console.error ( "y out of range");
					return;
				}
				box = this.elements.tbody.children[y].children[x];
			}

			if ( this.state != 'playing' &&
				 box.dataCaptured != false &&
				 this.props['player' + this.turn] != "human" ) return;
			box.dataCaptured = true;
			box.classList.add(tictactoe['className' + this.turn]);
			box.dataPlayer = this.turn;
		};

		this.reset = function ( a ) {
			this.elements.winLine.style.display = "none";
			this.props.sqcaptured = 0;
			this.elements.status.innerText = "";
			this.elements.playerSymbol.classList.remove('pX');
			this.elements.playerSymbol.classList.remove('pO');
			this.turn = this.props.beginPlayer;
			this.elements.playerSymbol.classList.add('p'+this.turn);

			for ( var i = 0; i < this.elements.tbody.children.length; i ++ ) {
				for ( var j = 0; j < this.elements.tbody.children[i].children.length; j ++ ) {
					this.elements.tbody.children[i].children[j].classList.remove('pX');
					this.elements.tbody.children[i].children[j].classList.remove('pO');
					this.elements.tbody.children[i].children[j].dataCaptured = false;
					this.elements.tbody.children[i].children[j].dataPlayer = '';
					this.elements.tbody.children[i].children[j].removeEventListener('click', clickBox);
				}
			}

			this.nextPlayer( true );
		};

		this.checkCurrentBox = function ( b ) {
			if ( b.dataPlayer == '' ) return false;
			var d = {
				'u'  : b,
				'ul' : b,
				'ur' : b,
				'l'  : b,
				'r'  : b,
				'd'  : b,
				'dl' : b,
				'dr' : b
			};

			var npe; // next parent element
			for ( var i = 0; i < this.props.linetowin - 1; i ++ ) {

				// DOWN
				if (d.d !== false ) {
					npe = d.d.parentElement.nextElementSibling;
					if ( npe === null ||
						 npe.children[d.d.cellIndex].dataPlayer != b.dataPlayer
					   ) d.d = false;
					else {
						d.d = npe.children[d.d.cellIndex];
					}
				}

				// UP
				if (d.u !== false ) {
					npe = d.u.parentElement.previousElementSibling;
					if ( npe === null ||
						 npe.children[d.u.cellIndex].dataPlayer != b.dataPlayer
					   ) d.u = false;
					else {
						d.u = npe.children[d.u.cellIndex];
					}
				}

				// RIGHT
				if (d.r !== false ) {
					npe = d.r.nextElementSibling;
					if ( npe === null ||
						 npe.dataPlayer != b.dataPlayer
					   ) d.r = false;
					else d.r = npe;
				}

				// LEFT
				if (d.l !== false ) {
					npe = d.l.previousElementSibling;
					if ( npe === null ||
						 npe.dataPlayer != b.dataPlayer
					   ) d.l = false;
					else d.l = npe;
				}

				// DOWN RIGHT
				if (d.dr !== false ) {
					npe = d.dr.parentElement.nextElementSibling;
					if ( npe === null ||
						 npe.children[d.dr.cellIndex].nextElementSibling === null ||
						 npe.children[d.dr.cellIndex].nextElementSibling.dataPlayer != b.dataPlayer
					   ) d.dr = false;
					else {
						d.dr = npe.children[d.dr.cellIndex].nextElementSibling;
					}
				}

				// DOWN LEFT
				if (d.dl !== false ) {
					npe = d.dl.parentElement.nextElementSibling;
					if ( npe === null ||
						 npe.children[d.dl.cellIndex].previousElementSibling === null ||
						 npe.children[d.dl.cellIndex].previousElementSibling.dataPlayer != b.dataPlayer
					   ) d.dl = false;
					else {
						d.dl = npe.children[d.dl.cellIndex].previousElementSibling;
					}
				}

				// UP RIGHT
				if (d.ur !== false ) {
					npe = d.ur.parentElement.previousElementSibling;
					if ( npe === null ||
						 npe.children[d.ur.cellIndex].nextElementSibling === null ||
						 npe.children[d.ur.cellIndex].nextElementSibling.dataPlayer != b.dataPlayer
					   ) d.ur = false;
					else {
						d.ur = npe.children[d.ur.cellIndex].nextElementSibling;
					}
				}

				// UP LEFT
				if (d.ul !== false ) {
					npe = d.ul.parentElement.previousElementSibling;
					if ( npe === null ||
						 npe.children[d.ul.cellIndex].previousElementSibling === null ||
						 npe.children[d.ul.cellIndex].previousElementSibling.dataPlayer != b.dataPlayer
					   ) d.ul = false;
					else {
						d.ul = npe.children[d.ul.cellIndex].previousElementSibling;
					}
				}

			}
			return { "b" : b, "directions" : d };
		};

		this.checkForWin = function () {
			var row, cell, r;
			for ( var y = 0; y < this.props.rows; y ++ ) {
				row = this.elements.tbody.children[y];
				for ( var x = 0; x < this.props.cols; x ++ ) {
					cell = row.children[x];
					r = this.checkCurrentBox(cell);
					if ( r !== false ) {
						for (var i in r.directions ) if ( r.directions[i] !== false ) {

							this.elements.winLine.style.display = "block";
							if ( i == "r" ) {
								let trf = r.b.parentNode;
								let wrow = Array.prototype.indexOf.call( trf.parentNode.children, trf);
								this.elements.winLine.style.left = "5px";
								this.elements.winLine.style.top = (-135 + wrow * 50) + "px";
								this.elements.winLine.style.width = "150px";
								this.elements.winLine.style.height = "0px";
								this.elements.winLine.style.border = "0px none";
								this.elements.winLine.style.borderBottom = "1px solid #000";
								this.elements.winLine.style.boxShadow = "0px 0px 3px 2px #000";
								this.elements.winLine.style.rotate = "0deg";
							} else if ( i == "d" ) {
								let wcol = Array.prototype.indexOf.call( r.b.parentNode.children, r.b);
								this.elements.winLine.style.left = (25 + ( wcol * 50 )) + "px";
								this.elements.winLine.style.top = "-155px";
								this.elements.winLine.style.width = "0px";
								this.elements.winLine.style.height = "150px";
								this.elements.winLine.style.border = "0px none";
								this.elements.winLine.style.borderRight = "1px solid #000";
								this.elements.winLine.style.rotate = "0deg";
							} else if ( i == "dr" ) {
								this.elements.winLine.style.width = "150px";
								this.elements.winLine.style.height = "0px";
								this.elements.winLine.style.border = "0px none";
								this.elements.winLine.style.borderBottom = "1px solid #000";
								this.elements.winLine.style.left = "-10px";
								this.elements.winLine.style.top = "-75px";
								this.elements.winLine.style.rotate = "45deg";
							} else if ( i == "ur" ) {
								this.elements.winLine.style.width = "180px";
								this.elements.winLine.style.height = "0px";
								this.elements.winLine.style.border = "0px none";
								this.elements.winLine.style.borderBottom = "1px solid #000";
								this.elements.winLine.style.left = "-10px";
								this.elements.winLine.style.top = "-75px";
								this.elements.winLine.style.rotate = "-45deg";
							}

							return {
								"symbol" : r.directions[i].dataPlayer,
								"direction" : i,
								"bbox" : r.b,
								"ebox" : r.directions[i]
							};
						}
							
					}
				}
			}
			return false;
		};

		this.reset();

	};

	tictactoe.className = "tictactoe";
	tictactoe.controlsClassName = "controls";
	tictactoe.playerSymbolClassName = "player-symbol";
	tictactoe.restartbtnClassName = "restartbtn";
	tictactoe.classNameX = "pX";
	tictactoe.classNameO = "pO";

	tictactoe.rows = 3;
	tictactoe.cols = 3;
	tictactoe.linetowin = 3;
	tictactoe.beginPlayer = 'X';

	tictactoe.onload = function () {
		let el = document.getElementsByTagName(tictactoe.className);
		for ( let i = 0; i < el.length; i ++ )
			if ( typeof el[i].dataTTTObject == 'undefined' )
				(new tictactoe ( el[i] ));
	};

	tictactoe.loadApp = function ( app ) {
		var ttt = new tictactoe();
		var w = lvzwebdesktop(app.container);

		if ( w !== false ) {
			w.attachItem(ttt.elements.container)
				.addEvent('itemattached', function(e){
					w.tictactoe = e.item.dataTTTObject;
				});

			if ( w.type == 'window' ) {
				w.setLayout('menu:minimize,close');
				w.disableResize();
				w.props.resizable = false;
				w.props.maximizable = false;
			}
			else if ( w.type == 'widget' ) {
			}

			w.setWidth(156);
			w.setHeight(229);
			w.resizeOnContent(false);
		}
		else {
			app.container.appendChild(tt.elements.container);
		}

	};

	function clickBox ( e ) {
		var self = e.target;
		if ( typeof self.dataTTTObject != 'object') return;
		self = self.dataTTTObject;
		self.captureBox( e.target );
		self.nextPlayer();
		var r = self.checkForWin();
		console.log ( r );
		if ( r !== false ) self.gameEnd(r);
	}

	window.addEventListener("load", function () {

		var el = document.getElementsByClassName(tictactoe.className)

		for ( var i = 0; i < el.length; i ++ ) new tictactoe ( el[i] );

	});

	var url = new URL(document.currentScript.src);
	var c = url.searchParams.get("$");
	if ( c !== null ) window[c].tictactoe = tictactoe;
	else window.tictactoe = tictactoe;

}) ();
